<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
Auth::routes();
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'StoreController@show');
Route::post('store/register/store', 'StoreController@register');

Route::group(['prefix' => 'system'], function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::get('register', "Admin\AuthController@register");
        Route::post('store', "Admin\AuthController@store");
        Route::get('login', "Admin\AuthController@login");
        Route::post('auth', "Admin\AuthController@auth");
    });

    Route::group(['prefix' => 'client'], function () {
        Route::post('register', "Client\AuthController@register");
        Route::post('login', "Client\AuthController@login");
    });
});

Route::group(['prefix' => '{store_name}', 'middleware' => ['bridge']], function () {
    Route::get('home', 'HomeController@home');
    Route::post('product/store', 'HomeController@store_product');

    Route::group(['prefix' => 'register'], function () {
        // Route::post('register/admin');
        // Route::post('register/client');
    });
});

Route::get('/home', 'HomeController@index')->name('home');
