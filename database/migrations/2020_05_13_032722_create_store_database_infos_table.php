<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreDatabaseInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_database_infos', function (Blueprint $table) {
            $table->id();
            $table->string("store_name");
            $table->string("template")->default("template1");
            $table->string("driver")->default("mysql");
            $table->string("url")->nullable();
            $table->string("host")->default("127.0.0.1");
            $table->string("port")->default("3306");
            $table->string("database");
            $table->string("username");
            $table->string("password");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_database_infos');
    }
}
