<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Cadastro de Loja </title>
    <style>
        .form {
            display: flex;
            width: 500px;
            height: 250px;
        }
    </style>
</head>
<body>
    <div class="form">
        <form action="<?= url('/store/register/store') ?>" method="post">
            <label for="store_name"> Nome da Loja </label>
            <input type="text" name="store_name" required> <br>

            <label for="template"> Template </label>
            <input type="text" name="template" required> <br>

            <br>
            <input type="submit" value="Registrar">
        </form>
    </div>
</body>
</html>