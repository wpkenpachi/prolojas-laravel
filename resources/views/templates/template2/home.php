<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Template2 </title>
</head>
<body>
    <h1> <?= $store_name ?> </h1>
    <hr>
    <form action="<?= url("{$store_name}/product/store"); ?>" method="post">
        <label for="name"> Nome do Produto </label>
        <input type="text" name="name"> <br>
        <label for="value"> Valor do Produto </label>
        <input type="number" name="value"> <br>

        <input type="submit">
    </form>
    <hr>
    <ul>
    <?php
        foreach ($products as $product) {
            echo "<li> {$product->name}: R$ {$product->value} </li>";
        }
    ?>
    </ul>
</body>
</html>