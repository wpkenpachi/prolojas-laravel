<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreDatabaseInfo extends Model
{
    protected $fillable = [
        'store_name',
        'template',
        'driver',
        'url',
        'host',
        'port',
        'database',
        'username',
        'password'
    ];
}
