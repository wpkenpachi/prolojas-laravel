<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Cliente extends Authenticatable
{
    use Notifiable;

    protected $table = 'clientes';
    protected $guard = 'cliente';

    protected $fillable = [
        'nome',
        'telefone',
        'cpf',
        'email',
        'senha'
    ];

    protected $hidden = [
        'senha'
    ];
}
