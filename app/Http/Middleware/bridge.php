<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\StoreDatabaseInfo;

class bridge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->store = StoreDatabaseInfo::where('store_name', $request->store_name)->first();
        if ($request->store) {
            return $next($request);
        } else {
            return response()->json([
                'msg' => 'Loja não existe na base de dados'
            ]);
        }
    }
}
