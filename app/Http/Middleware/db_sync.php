<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;
use App\Models\StoreDatabaseInfo;

class db_sync
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $databases = StoreDatabaseInfo::get();

        foreach ($databases as $store) {
            $store_name = $store->store_name;
            Config::set("database.connections.{$store_name}.driver", $store->driver);
            Config::set("database.connections.{$store_name}.url", null);
            Config::set("database.connections.{$store_name}.host", $store->host);
            Config::set("database.connections.{$store_name}.port", $store->port);
            Config::set("database.connections.{$store_name}.database", $store->database);
            Config::set("database.connections.{$store_name}.username", $store->username);
            Config::set("database.connections.{$store_name}.password", $store->password);
            Config::set("database.connections.{$store_name}.unixsocket", '');
            Config::set("database.connections.{$store_name}.charset", 'utf8');
            Config::set("database.connections.{$store_name}.prefix", '');
            Config::set("database.connections.{$store_name}.prefix_indexes", true);
            Config::set("database.connections.{$store_name}.schema", 'public');
            Config::set("database.connections.{$store_name}.sslmode", 'prefer');
            // Config::set("database.connections.{$store_name}.driver", $store->driver);
            // Config::set("database.connections.{$store_name}.url", null);
            // Config::set("database.connections.{$store_name}.host", $store->host);
            // Config::set("database.connections.{$store_name}.port", $store->port);
            // Config::set("database.connections.{$store_name}.database", $store->database);
            // Config::set("database.connections.{$store_name}.username", $store->username);
            // Config::set("database.connections.{$store_name}.password", $store->password);
            // Config::set("database.connections.{$store_name}.unixsocket", '');
            // Config::set("database.connections.{$store_name}.charset", 'utf8mb4');
            // Config::set("database.connections.{$store_name}.collation", 'utf8mb4_unicode_ci');
            // Config::set("database.connections.{$store_name}.prefix", '');
            // Config::set("database.connections.{$store_name}.prefix_indexes", true);
            // Config::set("database.connections.{$store_name}.strict", true);
            // Config::set("database.connections.{$store_name}.engine", null);
            // Config::set("database.connections.{$store_name}.options",
            // extension_loaded('pdo_mysql') ? array_filter([
            //     \PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            // ]) : []);
        }
        return $next($request);
    }
}
