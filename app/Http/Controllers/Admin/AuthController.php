<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Admin;
use App\Models\User;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $nivel = 2;

    public function login() {
        return view('system.Login');
    }

    public function auth(Request $request) {
        if ( Auth::attempt(['email' => $request->email, 'password' => $request->password]) ) {
            return response()->json(['msg' => 'Logado com sucesso']);
        }
        return response()->json(['email' => 'Email or password are wrong.']);
    }

    public function register () {
        return view('system.Register');
    }

    public function store (Request $request) {
        $new_user_validator = Validator::make($request->all(), [
            'nome'  => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($new_user_validator->fails()) {
            return $new_user_validator->errors();
        }

        // $new_admin          = new Admin();
        // $new_admin->nome    = $request->nome;
        // $new_admin->email   = $request->email;
        // $new_admin->password   = bcrypt($request->senha);
        // $new_admin->nivel   = $this->nivel;
        // $new_admin->save();

        $new_admin          = new User();
        $new_admin->nome    = $request->nome;
        $new_admin->email   = $request->email;
        $new_admin->password   = bcrypt($request->senha);
        $new_admin->save();

        if (!$new_admin) {
            return response()->json([
                'msgm' => 'Usuário não pode ser cadastrado'
            ]);
        }

        return response()->json($new_admin);
    }

    public function home() {

    }

    public function guard() {
        return Auth::guard('admin');
    }
}
