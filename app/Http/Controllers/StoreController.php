<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Models\StoreDatabaseInfo;
use Illuminate\Support\Facades\Artisan;
use App\Services\SyncConnections;

class StoreController extends Controller
{
    public function show () {
        return view('store.register');
    }

    public function register (Request $request) {
        $db = [
            'store_name'    => $request->get('store_name'),
            'driver'        => 'pgsql',
            'host'          => '127.0.0.1',
            'port'          => '5432',
            'database'      => $request->get('store_name'),
            'username'      => 'postgres',
            'password'      => 'postgres'
        ];
        DB::statement(DB::raw('CREATE DATABASE ' . $request->get('store_name')));
        StoreDatabaseInfo::create($db);
        SyncConnections::run();
        Artisan::call("migrate --path=/database/migrations/default --database={$request->get('store_name')}");
        DB::connection($request->get('store_name'))->table('products')->insert(
            ['name' => 'Produto 01', 'value' => 10]
        );
        return redirect("{$request->store_name}/home");
    }
}
