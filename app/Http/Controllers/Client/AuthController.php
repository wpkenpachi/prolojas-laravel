<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    protected $nivel = 0;

    public function login(Request $request) {
        if (auth()->guard('client')->attempt(['email' => $request->email, 'senha' => $request->password])) {
            dd(auth()->guard('client')->user());
        }
        return response()->json(['email' => 'Email or password are wrong.']);
    }

    public function register (Request $request) {

    }

    public function home() {

    }

    public function guard() {
        return Auth::guard('client');
    }
}
