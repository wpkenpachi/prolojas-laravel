<?php
namespace App\Services;

use App\Models\StoreDatabaseInfo;
use Illuminate\Support\Facades\Config;

class SyncConnections {
    public static function run () {
        $databases = StoreDatabaseInfo::get();
        foreach ($databases as $store) {
            $store_name = $store->store_name;
            Config::set("database.connections.{$store_name}.driver", $store->driver);
            Config::set("database.connections.{$store_name}.url", null);
            Config::set("database.connections.{$store_name}.host", $store->host);
            Config::set("database.connections.{$store_name}.port", $store->port);
            Config::set("database.connections.{$store_name}.database", $store->database);
            Config::set("database.connections.{$store_name}.username", $store->username);
            Config::set("database.connections.{$store_name}.password", $store->password);
            Config::set("database.connections.{$store_name}.unixsocket", '');
            Config::set("database.connections.{$store_name}.charset", 'utf8');
            Config::set("database.connections.{$store_name}.prefix", '');
            Config::set("database.connections.{$store_name}.prefix_indexes", true);
            Config::set("database.connections.{$store_name}.schema", 'public');
            Config::set("database.connections.{$store_name}.sslmode", 'prefer');
        }
    }
}